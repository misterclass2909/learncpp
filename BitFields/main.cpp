#include <iostream>

using namespace std;

int main()
{
	
	/*Work with bit fields*/

	//struct BitField
	//{
	//	//There is one byte (6 bits)
	//	//char a : 3;
	//	//char b : 3;

	//	//There is 4 bytes (one int - min 4 bytes)
	//	//int a : 3;
	//	//int b : 3;

	//	//There is 8 bytes (system give memory divisible by max type size. One int - 4 bytes, and system give for char also 4 bytes)
	//	int a : 3;
	//	char b : 3;
	//};

	//cout << sizeof(BitField) << endl;

	/*WOrk with unions*/

	//One union gives max type memory, one cell, for all variables in union.
	union TestUnion
	{
		// 4 bytes for a and b
		//int a;
		//int b;

		//Union can be used for data interpretation on different types
		int a;
		char b;
	};

	TestUnion un;
	un.a = 10;
	un.b = 'a';

	cout << un.a << endl;
	cout << un.b << endl;

	return 0;
}