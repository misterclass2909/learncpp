﻿// WeakPointers.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <memory>
#include <string>

using namespace std;

class Human
{
private:
    string name;
    //shared_ptr<Human> partner;      //Shared pointer is owner of object. It leads to circle
    weak_ptr<Human> partner;          //Weak pointer is not owner of object, only observer.

public:
    Human(const string& name) : name(name)
    {
        cout << "Human is " << name << endl;
    }

    friend bool PartnerUp(shared_ptr<Human>& sh1, shared_ptr<Human>& sh2)
    {
        if (!sh1 || !sh2) return false;
        
        sh1->partner = sh2;     //Circle - one human have pointer to another and nobody can't be erased!
        sh2->partner = sh1;     //We can use weak pointer to fix it!

        //sh1->partner->name    //Weak pointer have not object access!! We must convert that to shared pointer!

        cout << sh1->name << " now partnered with " << sh2->name << endl;
        return true;
    }

    const string& GetName() const
    {
        return name;
    }

    const shared_ptr<Human> GetPartner() const
    {
        return partner.lock();              //Convert weak pointer to shared pointer - for access to object!
    }

    ~Human()
    {
        cout << name << " is destroyed" << endl;
    }
};

int main()
{
    auto ivan = make_shared<Human>("Ivan");
    auto anton = make_shared<Human>("Anton");

    PartnerUp(ivan, anton);

    auto partner = ivan->GetPartner();
    cout << ivan->GetName() << " partner is " << partner->GetName() << endl;

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
