﻿// STL_Deque.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <iterator>
#include <algorithm>

#include <deque>        //Stack + queue

using namespace std;

void Display(int num)
{
    cout << num << ", ";
}

int main()
{
    deque<int> deq;

    if (deq.empty()) cout << "Empty!" << endl;
    else cout << "Not empty!" << endl;

    for (int i = 0; i < 10; i++)
    {
        deq.push_back(i);
        deq.push_front(i);
    }

    cout << "Max size is " << deq.max_size() << endl;
    cout << "Current size is " << deq.size() << endl;

    for_each(deq.begin(), deq.end(), Display);
    cout << endl;

    for_each(deq.rbegin(), deq.rend(), Display);
    cout << endl;

    deq.resize(10, 100);

    for_each(deq.begin(), deq.end(), Display);
    cout << endl;

    deq.insert(deq.begin() + 2, 3, 100);

    for_each(deq.begin(), deq.end(), Display);
    cout << endl;

    cout << deq.size() << endl;

    deq.shrink_to_fit();

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
