﻿// StackAndHeap.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

using namespace std;

class Memory
{
public:
    int* GetStack()
    {
        int stackInfo = 10; //This value is in stack and will be destroyed after function output
        cout << "Stack info is " << stackInfo << " | " << &stackInfo << endl;
        return &stackInfo;
    }

    int* GetHeap()
    {
        int* heapInfo = new int(20);  //This value is in HEAP. It will be destroyed only after programmer command!!
        cout << "Heap info is" << *heapInfo << " | " << heapInfo << endl;
        return heapInfo;
    }
};

int main()
{
    Memory* memory = new Memory();

    int* stackInfo = memory->GetStack();
    int* heapInfo = memory->GetHeap();

    cout << endl;

    cout << "Result stack info is " << *stackInfo << " | " << stackInfo << endl;
    cout << "Result heap info is" << *heapInfo << " | " << heapInfo << endl;

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
