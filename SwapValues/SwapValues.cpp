﻿// SwapValues.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

using namespace std;

void PrintConsoleValues(int a, int b)
{
    cout << "First value is " << a << endl;
    cout << "Second value is " << b << endl << endl;
}

//Default way to swap 2 variables
void DefaultSwap(int& a, int& b)
{
    int temp = a;
    a = b;
    b = temp;
}

//Simple math way to swap two integers 
void SimpleMathSwap(int& a, int& b)
{
    a += b; //Get a + b
    b = a - b; //First value go to second variable
    a -= b; //Second value go to the first variable
}

//Simple math way to swap two integers 
void BitSwap(int& a, int& b)
{
    a ^= b;
    b = a ^ b;
    a ^= b;
}

int main()
{
    int a = 1;
    int b = 3;
    PrintConsoleValues(a, b);

    //DefaultSwap(a, b);
    //SimpleMathSwap(a, b);
    BitSwap(a, b);
    PrintConsoleValues(a, b);
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
