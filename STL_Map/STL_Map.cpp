﻿// STL_Map.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

#include <map>
#include <unordered_map>

#include <algorithm>
#include <iterator>

using namespace std;

void Display(pair<int, char> value)
{
    cout << value.first << " = " << value.second << endl;
}

int main()
{
    map<int, char> mp;

    //Only unique values!
    for (int i = 2; i < 8; i++) mp.emplace(make_pair(i, char(65 + i)));
    for (int i = 2; i < 9; i++) mp.emplace(make_pair(i, char(65 + i)));

    for_each(mp.begin(), mp.end(), Display);

    //We can find element by key
    auto itFind = mp.find(3);
    cout << "Find: ";
    Display(*itFind);

    //Direct access to index element (by key)
    cout << "At(4): " << mp.at(4) << endl;

    /* Multimap */

    multimap<int, char> mtp;

    cout << "\nMultimap\n" << endl;

    //Non unique values!
    for (int i = 0; i < 5; i++) mtp.emplace(make_pair(i, char(65 + i)));
    for (int i = 0; i < 6; i++) mtp.emplace(make_pair(i, char(75 + i)));

    for_each(mtp.begin(), mtp.end(), Display);

    auto equalRange = mtp.equal_range(3);
    cout << "Equal Range:" << endl;
    for_each(equalRange.first, equalRange.second, Display);


    /* Unordered is equal as container set */
    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
