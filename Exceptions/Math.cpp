#include "Math.h"
#include "TextException.h"

float Math::Divide(const int n1, const int n2)
{
	try
	{
		if (n2 == 0) throw MathException(1, (char*)"Are you fu**ing serious? Divide on null??");
		if (n1 == 666 && n2 == 666) throw MathException(333, (char*)"Are you fu**ing serious? Numbers of devil!");
		return (float)n1 / n2;
	}
	catch (MathException exp)
	{
		exp.Display();
		return (float)NULL;
	}
	catch (TextException exp)
	{
		exp.Display();
		return (float)NULL;
	}
}

float Math::Multiply(const int n1, const int n2)
{
	try
	{
		if (n1 == 1 && n2 == 1) throw MathException(2, (char*)"One multiplied by one? WTF??");
		return (float)n1 * n2;
	}
	catch (MathException exp)
	{
		exp.Display();
		return (float)NULL;
	}
}
