#pragma once
#include <iostream>
#include "Exception.h"

class Math
{
	class MathException : public Exception
	{
	public:
		MathException(int number, char* msg)
		{
			this->number = number;
			this->msg = msg;
		}

		void Display() override
		{
			std::cout << "Math exception on number " << this->number << std::endl;
			std::cout << this->msg << std::endl;
		}
	};

public:
	static float Divide(const int n1, const int n2);
	static float Multiply(const int n1, const int n2);
};

