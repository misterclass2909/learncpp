#include <iostream>
#include "TextException.h"

TextException::TextException(int number, char* msg)
{
	this->number = number;
	this->msg = msg;
}

void TextException::Display()
{
	std::cout << "Text exception on number " << this->number << std::endl;
	std::cout << this->msg << std::endl;
}
