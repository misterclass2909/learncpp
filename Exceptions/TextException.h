#pragma once
#include "Exception.h"

class TextException : public Exception
{
public:
	TextException(int number, char* msg);
	void Display() override;
};

