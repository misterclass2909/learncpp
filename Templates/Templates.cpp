﻿// Templates.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

using namespace std;

template<typename ValueType>
class TArray
{
private:
	ValueType* values;
public:
	int length;

    TArray(ValueType* values, int length)
    {
		this->length = length;

		this->values = new int[length];

		for (int i = 0; i < length; i++)
		{
			this->values[i] = values[i];
		}
    }

	//Get element by index
	ValueType& operator[] (const int index)
	{
		return this->values[index];
	}

	~TArray()
	{
		delete[] this->values;
	}
};

int main()
{
    int arr[3] = { 1, 2, 3 };
    TArray<int>* tArr = new TArray<int>(arr, 3);
    cout << "First element is " << (*tArr)[0] << endl;
    (*tArr)[1] = 10;
    cout << "Second element is " << (*tArr)[1] << endl;
    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
