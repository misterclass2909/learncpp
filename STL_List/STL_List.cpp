﻿// STL_List.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <algorithm>
#include <iterator>

#include <list>

using namespace std;

void Display(int num)
{
    cout << num << ", ";
}

int main()
{
    list<int> l;

    for (int i = 0; i < 10; i++) l.emplace_back(i);

    for_each(l.begin(), l.end(), Display);
    cout << endl;

    /* Get 5-th element with advance (iterators have not access in container middle, only front and back) */

    auto begin = l.begin();
    advance(begin, 5);
    l.insert(begin, 3, 120);

    for_each(l.begin(), l.end(), Display);
    cout << endl;

    list<int> l2;
    l2.assign(5, 66);
       
    //Sort ASC
    l.sort(less<int>());

    //Sort DESC
    //l.sort(greater<int>());

    for_each(l.begin(), l.end(), Display);
    cout << endl;

    for_each(l2.begin(), l2.end(), Display);
    cout << endl;

    //Merge with predicate
    l.merge(l2);

    for_each(l.begin(), l.end(), Display);
    cout << endl;

    //Remove duplicates
    l.unique();

    for_each(l.begin(), l.end(), Display);
    cout << endl;
    
    
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
