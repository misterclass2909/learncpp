﻿// SmartPointers1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

using namespace std;

template<typename Type>
class SmartPointer
{
private:
    Type* m_obj;
public:
    SmartPointer(Type* obj) : m_obj(obj)
    {}

    Type* operator-> ()
    {
        return m_obj;
    }

    /* Access to inside object */

    Type& operator* ()
    {
        return *m_obj;
    }

    ~SmartPointer()
    {
        delete m_obj;
    }
};

class AnyClass
{
public:
    int* arr;
    int length;

    void Init()
    {
        length = 10;
        arr = new int[length];
        for (int i = 0; i < length; i++) arr[i] = i;
    }

    ~AnyClass()
    {
        delete[] arr;
    }
};

int main()
{
    while (1)
    {
        SmartPointer<AnyClass> pointer(new AnyClass());
        pointer->Init();

        //Memory leak
        /*AnyClass* obj = new AnyClass();
        obj->Init();*/
    }
    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
