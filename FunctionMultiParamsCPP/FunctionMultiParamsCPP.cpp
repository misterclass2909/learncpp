﻿
#include <iostream>
#include <stdarg.h>

using namespace std;

void MultiParamFunc(int num, int first, ...)
{
    //int* pointer = &first;

    //while (num--)
    //{
    //    cout << *pointer++ << endl;
    //    //pointer += 1;
    //}

    /*for (int* ptr = &first; num > 0; num--)
    {
        cout << *ptr++ << endl;
    }*/

    //Arguments list
    va_list args;

    //Get ready to arguments foreach (pointer on first element)
    va_start(args, num);

    for (; num > 0; num--)
    {   
        //Get current argument and inc pointer to next element
        int result = va_arg(args, int);
        cout << result << endl;
    }

    //Correct arguments work close
    va_end(args);
}

int main()
{
    MultiParamFunc(6, 10, 20, 30);
    return 0;
}

