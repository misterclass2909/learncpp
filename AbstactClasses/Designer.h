#pragma once
#include "Employee.h"
class Designer : public Employee
{

public:
	Designer() : Employee()
	{}
	void Work() override;
	void Design();
};

