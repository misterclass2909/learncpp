#pragma once
#include "Employee.h"

class Programmer : public Employee
{

public:
	Programmer() : Employee()
	{}
	void Work() override;
	void Coding();
};

