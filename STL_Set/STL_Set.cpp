﻿// STL_Set.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

#include <algorithm>
#include <iterator>

#include <set>
#include <unordered_set>

using namespace std;

void Display(int num)
{
    cout << num << ", ";
}


int main()
{
    set<int> st;        //Set is for unique values!
    
    if (st.empty()) cout << "Is empty" << endl;
    else cout << "Is not empty." << endl;

    for (int i = 0; i < 5; i++) st.emplace(i);

    //used autosort
    st.emplace_hint(st.begin(), 10);
    st.emplace_hint(st.end(), 20);

    for_each(st.begin(), st.end(), Display);
    cout << endl;

    st.emplace(7);

    for_each(st.begin(), st.end(), Display);
    cout << endl;

    //Try push non-unique elements
    for (int i = 0; i < 5; i++) st.emplace(i);
    for_each(st.begin(), st.end(), Display);
    cout << endl;

    //Binary find

    auto found = st.find(10);
    if (found != st.end()) cout << *found << endl;
    else cout << "Not found!" << endl;


    /* Multiset */

    cout << "\nMultiset\n" << endl;

    multiset<int> mst;

    //Multiset can push not unique values
    for (int i = 0; i < 5; i++) mst.emplace(i);
    for (int i = 0; i < 6; i++) mst.emplace(i);

    for_each(mst.begin(), mst.end(), Display);
    cout << endl;

    auto eqr = mst.equal_range(3);
    cout << "Equal range ";
    int length = 0;
    for (auto i = eqr.first; i != eqr.second; i++) length++;
    cout << "We have " << length << " elements in range" << endl;

    /* Unordered multiset */

    cout << "\nUnordered multiset\n" << endl;

    unordered_multiset<int> umst;

    for (int i = 0; i < 5; i++) umst.emplace(i);
    for (int i = 0; i < 6; i++) umst.emplace(i);

    for_each(umst.begin(), umst.end(), Display);
    cout << endl;

    cout << "Bucket count is " << umst.bucket_count() << endl;
    cout << "Size of 3-bucket is " << umst.bucket_size(3) << endl;
    cout << "Bucket num for element 3 is " << umst.bucket(3) << endl;           //Get hash-number
    cout << "Bucket num for element 100 is " << umst.bucket(100) << endl;

    /* Hash numbers example */

    unordered_multiset<char*> charUms;
    auto fn = charUms.hash_function();

    cout << "Helo: " << fn((char*)"Helo") << endl;
    cout << "Hela: " << fn((char*)"Hela") << endl;

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
