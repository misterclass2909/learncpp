#include <iostream>
#include <queue>

using namespace std;

int main()
{
	queue<char*> q;

	q.push((char*)"String 1");
	q.emplace((char*)"String 2");
	q.push((char*)"String 3");

	if (q.empty()) cout << "Queue is empty!" << endl;
	else cout << "Queue has " << q.size() << " size!" << endl;

	cout << q.front() << endl;
	cout << q.back() << endl;

	q.pop();

	cout << q.front() << endl;
	cout << q.back() << endl;

	q.pop();

	cout << q.front() << endl;
	cout << q.back() << endl;

	cout << endl;

	priority_queue<int> prq;

	prq.emplace(15);
	prq.emplace(20);
	prq.emplace(30);
	prq.emplace(5);

	cout << prq.top() << endl;
	prq.pop();

	cout << prq.top() << endl;
	prq.pop();
	cout << prq.top() << endl;
	prq.pop();
	cout << prq.top() << endl;
	prq.pop();
	return 0;
}