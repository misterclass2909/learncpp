﻿// DynamicCasts.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <typeinfo>

using namespace std;

class Base
{
     public:
         virtual void Test() = 0;
};

class C : public Base
{
public: 
    void Test() override 
    {}
};

class D : public C
{
    void Test() override
    {}
};

class Other
{
public:
    virtual void foo()
    {}
};

int main()
{
    D* objectD = new D();
    cout << typeid(objectD).name() << endl;

    C* objectC = objectD;
    cout << typeid(objectC).name() << endl;

    Base* objectBase = objectC;
    cout << typeid(objectBase).name() << endl;

    /* You can cast only polymorph types! (with virtual methods)*/

    //D* newObjectD = (D*)objectBase; //Can work without virtual, not safe!          
    D* newObjectD = dynamic_cast<D*>(objectBase);   //Only with virtual, safe
    cout << typeid(newObjectD).name() << endl;


    /* Check object type */

    Other* other = new Other();

    //Base* firstTest = (Base*)other;                   //Not safe check. Other object convert to Base, but it isn't Base. Tha't mistake!
    Base* firstTest = dynamic_cast<Base*>(other);       //Safe check. Other object is not Base, and you can detect it with dynamic cast.
    if (firstTest) cout << "Base" << endl;
    else cout << "Other" << endl;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
