﻿// Pairs.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <utility>

using namespace std;

int main()
{
    //Pair - two values, for example (x,y) coords
    /*pair<int, int> coord(1, 2);
    cout << coord.first << endl;
    cout << coord.second << endl;*/

    //Reduce pair declaration with typedef
    typedef pair<int, int> coords;
    coords testCoord(1, 10);
    cout << testCoord.first << endl;
    cout << testCoord.second << endl;

    //Use make pair function
    testCoord = make_pair(1, 15);
    cout << testCoord.first << endl;
    cout << testCoord.second << endl;

    coords coord1(1, 2);
    coords coord2(2, 2);

    if (coord1 != coord2) cout << "Comfortable comprasion two pairs!" << endl;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
