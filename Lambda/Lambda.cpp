﻿// Lambda.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

#include <algorithm>

//Functions declaration
#include <functional>

using namespace std;

int main()
{
    //Simple lambda expression

    auto test1 = []()
    {
        static int num = 0;
        cout << "Hello, num is " << num++ << endl;
        return "Hello";
    };

    cout << test1() << endl;

    //Not auto definition
    function<int()> test2 = []()
    {
        return 1 + 1;
    };

    cout << "Test 2: " << test2() << endl;

    //with params
    function<int(int, int)> test3 = [](int x, int y)
    {
        return x + y;
    };

    cout << "Test 3: " << test3(5, 5) << endl;

    //Void type
    function<void(int, int)> test4 = [](int x, int y)
    {
        cout << "Test 4: " << x + y << endl;
    };

    test4(1, 2);

    /* With global variables - lambda is functor and can save own statement */

    int integer1 = 100, integer2 = 100;

    function<int()> test5 = [integer1, &integer2]()
    {
        return integer1 + integer2;
    };

    cout << "Test 5(1): " << test5() << endl;   //200

    integer1 = integer2 = 1;

    cout << "Test 5(2): " << test5() << endl;   //101

    /* Fast lambda call */

    int test6 = [](int x, int y) { return x + y; } (11, 22);
    cout << "Test 6: " << test6 << endl;  

    /* Using example */

    int arr[] = { 1, 2, 3, 4, 5 };
    cout << "Test7: ";
    for_each(arr, arr + 5, [](int el) { cout << "," << el; });
    
    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
