﻿// STL_Arrays.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

#include <array>
#include <algorithm>
#include <iterator>

using namespace std;

void Display(char* str)
{
    cout << str << endl;
}

int main()
{
    array<char*, 3> arr;

    //size() and max_size() will return same value?? WTF?????
    cout << "Current size " << arr.size() << endl;
    cout << "Max size " << arr.max_size() << endl;

    //In array definition empty() ALWAYS return false?? WTF????
    if (arr.empty()) cout << "He is empty!" << endl;
    else cout << "Not empty" << endl;

    arr[0] = (char*)"String 1";
    arr.at(1) = (char*)"String 2";
    arr.at(2) = (char*)"String 3";

    /* Can be used auto keyword for auto type definition */

    //array<char*, 3>::iterator beginIterator = arr.begin();
    auto beginIterator = arr.begin();
    auto endIterator = arr.end();

    for_each(beginIterator, endIterator, Display);
    cout << endl;

    //Fill array with one value
    arr.fill((char*)"Test value");

    auto rBeginIterator = arr.rbegin();
    auto rEndIterator = arr.rend();

    for_each(rBeginIterator, rEndIterator, Display);

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
