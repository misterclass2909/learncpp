﻿// STL_Stack.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

#include <stack>

using namespace std;

int main()
{
    stack<char*> stack;

    stack.push((char*)"String 1");
    stack.push((char*)"String 2");
    stack.push((char*)"String 3");

    /* LIFO */

    cout << stack.top() << endl;

    stack.pop();
    cout << stack.top() << endl;

    stack.emplace((char*)"MIster class");
    cout << stack.top() << endl;

    /* Swap test */

    std::stack<char*> rightStack;

    rightStack.push((char*)"Right 1");
    rightStack.push((char*)"RIght 2");

    stack.swap(rightStack);

    cout << stack.top() << endl;
    cout << rightStack.top() << endl;
    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
