﻿// STL_SmartPointers.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <vector>
#include <algorithm>    
#include <memory>

using namespace std;

struct Foo
{
    Foo()
    {
        cout << "Foo init" << endl;
    }

    void show()
    {
        cout << "Hello from Foo: " << this << endl;
    }

    ~Foo()
    {
        cout << "~Foo destruct!" << endl;
    }
};

class DeleteFoo
{
public:
    //Used for delete object functor in smart pointers
    void operator()(Foo* obj) const
    {
        cout << "Call delete for Foo obj!" << endl;
        delete obj;
    }
};

int main()
{
    //Unique pointers
    {
        unique_ptr<Foo> ptr1(new Foo());
        if (ptr1) ptr1->show();             //Like simple pointers - can used in bool

        //Move unqiue pointers! (Unique pointers can not be copied!!)
        {
            unique_ptr<Foo> ptr2(move(ptr1));
            if (!ptr1) cout << "First pointer is EMPTY!" << endl;           //Ptr1 come to ptr2, ptr1 now is empty

            //Revert

            ptr1 = move(ptr2);
            if (!ptr2) cout << "Second pointer is EMPTY!" << endl;          //Ptr1 was back!

            //No copy!
            //ptr1(ptr2);
        }
    }

    //Shared pointer
    cout << "\nShared pointers:\n" << endl;
    {
        shared_ptr<Foo> sh1; //Empty pointer;

        //Constructor with object
        shared_ptr<Foo> sh2(new Foo());
        //auto sh3 = std::make_shared<Foo>();

        //Copy pointer
        shared_ptr<Foo> sh3(sh2);

        cout << "First pointer using: " << sh2.use_count() << endl;
        cout << "Second pointer using: " << sh3.use_count() << endl;

        //2 method calls to one object!
        sh2->show();
        sh3->show();

        //Use special object from target obj destruction - make functor and enjoy!
        shared_ptr<Foo> sh4(new Foo(), DeleteFoo());
        //shared_ptr<Foo> sh5(new Foo(), default_delete<Foo>()); //By default
    }

    //Functional object - default_delete
    {
        //Call default_delete<int>
        unique_ptr<int> ptr1(new int(5));

        //Call default_delete<int[]>
        unique_ptr<int[]> ptr2(new int[10]);  //Int[] to template typename!!
        unique_ptr<int[]> ptr3(new int[10], default_delete<int[]>());

        //Default_delete is functor and can be used anywhere!
        vector<int*> v;
        for (int i = 0; i < 100; i++) v.push_back(new int(i));
        for_each(v.begin(), v.end(), default_delete<int>());
    }

    //Weak ptr example is another project. Weak pointer can store the object, but can not be object owner! (используется для решения циклической зависимости)
    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
