#pragma once

#include <iostream>
#include <string>

using namespace std;

class Phone
{
private:

	mutable std::string model;
	int year;

	const std::string producer;

public:	
	
	/*Constructors*/

	Phone(std::string model, int year);

	/*Getters*/

	std::string GetModel() const;
	int GetYear();

	/*Setters*/
	
	void SetModel(std::string model) const;
	void SetYear(int year);

	friend ostream& operator << (ostream& s, const Phone& p);

	void Show() const;
};

