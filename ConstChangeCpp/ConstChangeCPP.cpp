﻿#include <iostream>
#include "Phone.h"

using namespace std;

int main()
{
	const Phone phone("Test model", 2008);

	cout << phone;

	phone.SetModel("Iphone 11");

	phone.Show();

	return 0;
}