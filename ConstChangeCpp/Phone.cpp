#include "Phone.h"

//Phone::Phone(std::string model, int year)
//{
//	this->model = model;
//	this->year = year;
//}

//That constructor is used for references and constants
//Phone::Phone(std::string model, int year) : model(model), year(year), producer("Apple")
//{
//
//}

Phone::Phone(std::string model, int year) :  producer("Apple")
{
	this->model = model;
	this->year = year;
}

std::string Phone::GetModel() const
{
	return this->model;
}

int Phone::GetYear()
{
	return this->year;
}

void Phone::SetModel(std::string model) const
{
	this->model = model;
}

void Phone::SetYear(int year)
{
	this->year = year;
}

ostream& operator<<(ostream& s, const Phone& p)
{
	s << "Model is " << p.model << "\n";
	s << "Year is " << p.year << "\n";
	return s;
}

void Phone::Show() const
{
	cout << "Model is " << this->model << "\n";
	cout << "Year is " << this->year << "\n";
}
