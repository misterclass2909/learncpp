﻿// PointersAndObjectsSort.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <ctime>
#include "Object.h"

using namespace std;

//Got Quick sort
void SwapPointers(Object*& obj1, Object*& obj2)
{
    Object* temp = obj1;
    obj1 = obj2;
    obj2 = temp;
}

//For Slow sort
void SwapObjects(Object*& obj1, Object*& obj2)
{
    Object temp = *obj1;
    *obj1 = *obj2;
    *obj2 = temp;
}

//Bubble sort
void BubbleSort(Object** objects, int length, void (*swap)(Object*&, Object*&))
{
    for (int i = 0; i < length - 1; i++)
    {
        bool isSwapped = false;
        
        for (int j = 0; j < length - i - 1; j++)
        {
            if (objects[j]->data1 > objects[j + 1]->data1)
            {
                swap(objects[j], objects[j + 1]);
                isSwapped = true;
            }

            //If not swapped, go forward
            if (!isSwapped) break;
        }
    }
}

int main()
{
    void (*quickSwap)(Object*&, Object*&) = &SwapPointers;
    void (*slowSwap)(Object*&, Object*&) = &SwapObjects;

    const int count = 20000;

    Object* objects[count];

    for (int i = 0; i < count; i++)
    {
        objects[i] = new Object(count - i, count - i, count - i, count - i);
    }

    //BubbleSort(objects, count, quickSwap);
    BubbleSort(objects, count, slowSwap);

    //Display last 50 elements
    for (int i = count - 50; i < count; i++)
    {
        objects[i]->Display();
    }

    cout << clock() << endl;

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
