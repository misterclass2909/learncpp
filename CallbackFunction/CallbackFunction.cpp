﻿// CallbackFunction.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

void LogFile(const char* msg)
{
    ofstream fout("log.txt", ios_base::app);

    if (fout.is_open())
    {
        fout << "Log is " << msg << endl;
        cout << "Message is logged" << endl;
        fout.close();
    }
    else
    {
        cout << "Error in file opening!" << endl;
    }
}

void LogConsole(const char* msg)
{
    cout << "Log message is " << msg << endl;
}

void Sum(int a, int b, void (*f)(const char* msg))
{
    int res = a + b;

    //Format output
    stringstream stream;
    stream << "Your result of " << a << " + " << b << " is " << res;

    //Get char* message
    const char* msg = _strdup(stream.str().c_str());

    //Log result
    f(msg);
}

int main()
{
    /*LogConsole("Test");
    LogFile("Test2");*/

    void (*consoleLogger)(const char* msg) = &LogConsole;
    void (*fileLogger)(const char* msg) = &LogFile;

    Sum(5, 5, consoleLogger);
    Sum(7, 8, fileLogger);
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
