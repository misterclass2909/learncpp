#pragma once
class HeavyFunctor
{
private:
	int evenSum;
	int oddSum;
public:
	HeavyFunctor() : evenSum(0), oddSum(0)
	{}

	//Here we create functor with parameter!
	void operator()(int num)
	{
		if (num % 2 == 0) evenSum++;
		else oddSum++;
	}

	int GetEvenSum()
	{
		return evenSum;
	}

	int GetOddSum()
	{
		return oddSum;
	}
};

