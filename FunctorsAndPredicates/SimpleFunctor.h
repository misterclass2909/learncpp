#pragma once
#include <iostream>

using namespace std;

template <typename T> 
class SimpleFunctor
{
private:
	T name;
public:
	SimpleFunctor(const T name) : name(name)
	{}

	//Functor operator - we can use this object like function!!!
	void operator()()
	{
		cout << "Name is " << name << endl;
	}
};

