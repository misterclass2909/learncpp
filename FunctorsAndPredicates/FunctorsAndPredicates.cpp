﻿// FunctorsAndPredicates.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
#include "SimpleFunctor.h"
#include "HeavyFunctor.h"

using namespace std;

bool evenPredicate(int x)
{
    return x % 2 == 0;
}

int main()
{
    /* Simple functor - just a function call! */
    SimpleFunctor<string> functor("MisterClass");
    functor();

    /* Heavy functor - work with parameters and object statement */

    HeavyFunctor obj;
    int list[]{ 0, 1, 2, 3, 4, 5, 6, 7, 8 };

    for (int el : list)
    {
        obj(el);
    }

    cout << "Even sum is " << obj.GetEvenSum() << endl;
    cout << "Odd sum is " << obj.GetOddSum() << endl;

    for (int el : list)
    {
        cout << el << " - " << ((evenPredicate(el)) ? "even" : "odd") << endl;
    }

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
