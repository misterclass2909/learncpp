﻿// ForeachCPP.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

void Display(int value)
{
    cout << value << ", ";
}

int main()
{
    const int length = 10;
    int arr[length] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    /* Index container for each */
    //for (int i = 0; i < length; i++) Display(arr[i]);

    /* For each with : construction */
    //for (int element : arr) Display(element);     //With detected elementy type
    //for (auto element : arr) Display(element);      //With auto type
    //for (int& element : arr) Display(element);        //With element reference for update array or performance

    /* With STL-container */

    //vector<int> vec({ 1, 2, 3 });
    //for (int& element : vec) Display(element);

    /* You can't work with dynamic arrays */

    //int* arrPointer = new int[3]{ 1, 2, 3 };
    //for (int element : *arrPointer) Display(element);   //There is compilation error

    /* You can work with algorithm for_each */

    vector<int>* vec = new vector<int>({ 1, 2, 3, 4, 5 });
    vector<int>::iterator begin = vec->begin();
    vector<int>::iterator end = vec->end();


    for_each(begin, end, Display);

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
