﻿// STL_Vectors.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

void Display(char* str)
{
    cout << str << endl;
}

void ForEachVector(const vector<char*> arr)
{
    auto beginIterator = arr.begin();
    auto endIterator = arr.end();
    for_each(beginIterator, endIterator, Display);
    cout << endl;
}


int main()
{
    vector<char*> vec;

    cout << "Max size is " << vec.max_size() << endl;
    cout << "Current size is " << vec.size() << endl;
    cout << "Capacity is " << vec.capacity() << endl;

    if (vec.empty()) vec.push_back((char*)"String 1");      //Push first element
    vec.resize(3, (char*)"Resize string");                  //Make size-3 vector and fill unused memory with value (used values are saved)

    cout << "Current size is " << vec.size() << endl;
    cout << "Capacity is " << vec.capacity() << endl;

    ForEachVector(vec);

    cout << "Now we make reserve and shrink\n" << endl;

    vec.reserve(7);     //Setup first 7 ready for fill cells

    cout << "Current size is " << vec.size() << endl;
    cout << "Capacity is " << vec.capacity() << endl;

    vec.shrink_to_fit();        //Shrink capacity to vector size

    cout << "Current size is " << vec.size() << endl;
    cout << "Capacity is " << vec.capacity() << endl;

    cout << "Now we make inserts!\n" << endl;

    vec.insert(vec.begin() + 2, 3, (char*)"Insert string");     //Insert used much methods definitions
    ForEachVector(vec);

    vec.emplace(vec.begin() + 5, (char*)"Emplace string");      //Emplace used for one value inserting, it's faster
    ForEachVector(vec);

    cout << "Current size is " << vec.size() << endl;
    cout << "Capacity is " << vec.capacity() << endl;

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
