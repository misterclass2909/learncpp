﻿// Iterators.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

int main()
{
    typedef vector<int> vint;
    vint v1;

    for (int i = 0; i < 15; i++) v1.emplace_back(i);

    /* Forward iterators */

    cout << "Forward iterators: ";

    for (
        auto i = v1.begin();
        i != v1.end() - 7;          //Random access iterator^)
        i++
        )
    {
        *i *= 2;                   //Multiply value by 2
        cout << ", " << *i;
    }
    cout << endl;

    /* Reverse iterators */

    cout << "Backward iterators: ";

    for (auto i = v1.rbegin(); i != v1.rend() - 7; i++)
    {
        *i *= 2;                   //Multiply value by 2
        cout << ", " << *i;
    }

    cout << endl;

    /* Const iterators  */

    cout << "Const iterators: ";

    for (auto i = v1.cbegin(); i != v1.cend() - 7; i++)
    {
        //*i *= 2;                   //We can not change iterator value
        cout << ", " << *i;
    }
    cout << endl;

    /* Iterator operations */

    //Advance iterator on [param] positions with sign
    cout << "\nAdvance:\n";
    auto advanceIt = v1.begin();
    advance(advanceIt, 5);      cout << *advanceIt << endl;     //Move forward on 5 elements
    advance(advanceIt, -2);      cout << *advanceIt << endl;     //Move backward on 2 elements

    //Distanse - elements count between two iterators
    cout << "\nDistanse:\n";
    auto distanseBegin = v1.begin();
    auto distanseEnd = v1.end();
    cout << distance(distanseBegin, distanseEnd) << endl;

    //Iter_swap - SWAP ELEMENTS IN CONTAINER by iterators
    cout << "\nIterators swap:\n";
    auto swapBegin = v1.begin();
    auto swapEnd = v1.end() - 1;        //Get last element with value
    iter_swap(swapBegin, swapEnd);
    cout << "Now begin is " << *swapBegin << ", but end is " << *swapEnd << endl;

    //Begin
    cout << "\nBegin:\n";
    auto beg = v1.begin() + 3;
    cout << "Before is " << *beg << endl;
    beg = begin(v1);
    cout << "After is " << *beg << endl;

    //End
    cout << "\nEnd:\n";
    auto en = v1.end() - 3;
    cout << "Before is " << *en << endl;
    en = end(v1) - 1;
    cout << "After is " << *en << endl;

    //Prev - move iterator backward on [param] elements
    cout << "\nPrev:";
    auto prevEnd= v1.end();
    prevEnd = prev(prevEnd, 1);
    cout << *prevEnd << endl;

    //Next - move iterator forward on [param] elements
    cout << "\nNext:";
    auto nextBegin = v1.begin();
    nextBegin = next(nextBegin, 1);
    cout << *nextBegin << endl;

    /* Iterator generators */

    //back_inserter or front_inserter   
    {
        vint foo, bar;
        for (int i = 1; i <= 5; i++)
        {
            foo.emplace_back(i);
            bar.emplace_back(i * 10);
        }

        cout << "\nBack inserter";
        //copy(foo.begin(), foo.end(), bar);                                        //We can not write it
        copy(bar.begin(), bar.end(), back_inserter(foo));                           //Copy from bar to foo, in the end

        for (int i : foo) cout << ", " << i;
        cout << endl;
    }

    //Inserter
    {
        vint vect{ 1, 2, 3, 4, 5 };

        fill_n(inserter(vect, vect.begin() + 1), 3, -1);         //Push 3 elements (-1) on second position

        cout << "\nInserter:";
        for (int el : vect) cout << ", " << el;
        cout << endl;
    }

    //Move iterators
    {
        vector<string> def{ "One", "Two", "Three" };

        vector<string> vstr1(def.begin(), def.end());       //Copy def to vstr1
        vector<string> vstr2(make_move_iterator(def.begin()), make_move_iterator(def.end()));           //MOVE def to vstr2

        cout << "\nMove iterators:\n" << endl;

        cout << "Vstr1: ";
        for (auto str : vstr1) cout << str << ",";
        cout << endl;

        cout << "Vstr2: ";
        for (auto str : vstr2) cout << str << ",";
        cout << endl;

        //Interesting - move iterators moves values from source, but left empty strings!! Size = 3! WTF!

        def.shrink_to_fit();

        cout << "Default: ";
        for (auto str : def) cout << str << ",";
        cout << endl;
    }

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
